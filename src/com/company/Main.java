package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Введите количество строк: ");
        int kolStrok = new Scanner(System.in).nextInt();
        String[] arr = new String[kolStrok];
        for (int i = 0; i < kolStrok; i++) {
            System.out.println("Введите " + (i + 1) + "ю строку");
            arr[i] = new Scanner(System.in).nextLine();
        }
        String minArr = arr[0];
        for (int j = 0; j < kolStrok; j++) {
            if (minArr.length() > arr[j].length()) {
                minArr = arr[j];
            }
        }
        System.out.println("Самая короткая строка: " + minArr);
        System.out.println("Её длина = " + minArr.length());
    }
}